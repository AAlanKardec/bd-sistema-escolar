-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: sistemaescolar
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbemprestimo`
--

DROP TABLE IF EXISTS `tbemprestimo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbemprestimo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idlivro` varchar(45) DEFAULT NULL,
  `nomeLivro` varchar(45) DEFAULT NULL,
  `periodo` varchar(45) DEFAULT NULL,
  `codbarras` varchar(45) DEFAULT NULL,
  `turma` varchar(45) DEFAULT NULL,
  `sala` varchar(45) DEFAULT NULL,
  `aluno` varchar(45) DEFAULT NULL,
  `dataemprestimo` varchar(45) DEFAULT NULL,
  `dataDevolucao` varchar(45) DEFAULT NULL,
  `situacao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1 COMMENT='nomeLivro,periodo,turma,sala,aluno,dataEntrga,dataDevolucao';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbemprestimo`
--

LOCK TABLES `tbemprestimo` WRITE;
/*!40000 ALTER TABLE `tbemprestimo` DISABLE KEYS */;
INSERT INTO `tbemprestimo` VALUES (17,'3','A Paixao de Cristo','Tarde','123456789','Informatica Tarde A','Info T A','Alan Kardec','22 / 22 / 2222','09 / 13 / 9876','Devolvido'),(25,'4','senhor dos Aneis','Manha','987654321','Informatica Noite A','Info N A','Alan','22 / 22 / 2222','99 / 99 / 9999','Devolvido'),(40,'5','Assassin\'s Creed','Manha','3212345654','Informatica Manha A','data','aluno data','4/7/17','4/7/17','Devolvido'),(41,'5','Assassin\'s Creed','Manha','3212345654','Informatica Manha A','Info N A','Kenia Natiele','4/7/17','4/7/17','Devolvido');
/*!40000 ALTER TABLE `tbemprestimo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-07 23:57:00
