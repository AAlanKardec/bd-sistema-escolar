-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: sistemaescolar
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbnotas`
--

DROP TABLE IF EXISTS `tbnotas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbnotas` (
  `idnotas` int(11) NOT NULL AUTO_INCREMENT,
  `idaluno` varchar(45) DEFAULT NULL,
  `nomealuno` varchar(45) DEFAULT NULL,
  `prova1` double DEFAULT NULL,
  `prova2` double DEFAULT NULL,
  `trabalho1` double DEFAULT NULL,
  `trabalho2` double DEFAULT NULL,
  `extra1` double DEFAULT NULL,
  `extra2` double DEFAULT NULL,
  `turma` varchar(45) DEFAULT NULL,
  `curso` varchar(45) DEFAULT NULL,
  `disciplina` varchar(45) DEFAULT NULL,
  `situacao` varchar(45) DEFAULT NULL,
  `totaldisc` varchar(45) DEFAULT NULL,
  `nomeprof` varchar(45) DEFAULT NULL,
  `dataalteracao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idnotas`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 COMMENT='nomealuno,prova,trabalho,extra,turma,curso,disciplina';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbnotas`
--

LOCK TABLES `tbnotas` WRITE;
/*!40000 ALTER TABLE `tbnotas` DISABLE KEYS */;
INSERT INTO `tbnotas` VALUES (9,'5','Alan Kardeck',10,10,0,5,0,0,'Turma InfoA','Informática','Redes','Reprovado','25.0',NULL,NULL),(10,'6','Thayna Silva',5,10,0,0,0,0,'Turma InfoA','Informática','Redes','Reprovado','15.0',NULL,NULL),(11,'9','Graciele Silva',0,0,0,0,0,0,'Turma InfoA','Informática','Redes','Reprovado','',NULL,NULL),(12,'10','Wendreus',1,0,0,0,0,0,'Turma InfoA','Informática','Redes','Reprovado','1.0',NULL,NULL),(13,'1','Joao Paulo da silva',12,35,20,5,2,5,'Informatica Noite A','Informatica','Animacao 3D','Aprovado','79.0',NULL,NULL),(14,'3','maria eugenia',25,22,20,21,1,1,'Informatica Tarde A','Informatica','Animacao 3D','Aprovado','90.0',NULL,NULL),(15,'1','Joao Paulo da silva',15,20,24,22,2,1,'Informatica Noite A','Informatica','Redes','Aprovado','84.0','Administrador','4/7/17');
/*!40000 ALTER TABLE `tbnotas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-07 23:57:00
