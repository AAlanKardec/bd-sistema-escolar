-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: sistemaescolar
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbaluno`
--

DROP TABLE IF EXISTS `tbaluno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbaluno` (
  `idaluno` int(11) NOT NULL AUTO_INCREMENT,
  `dataaluno` varchar(45) NOT NULL DEFAULT 'CURRENT_DATE()',
  `cpfaluno` varchar(45) NOT NULL,
  `nomealuno` varchar(45) NOT NULL,
  `datanasc` varchar(45) NOT NULL,
  `rgaluno` varchar(45) NOT NULL,
  `sexoaluno` varchar(45) NOT NULL,
  `estcivilaluno` varchar(45) NOT NULL,
  `situacaoaluno` varchar(45) NOT NULL,
  `emailaluno` varchar(45) NOT NULL,
  `telaluno` varchar(45) NOT NULL,
  `nacionalidadedaluno` varchar(45) NOT NULL,
  `cepaluno` varchar(45) NOT NULL,
  `cidadealuno` varchar(45) NOT NULL,
  `bairroaluno` varchar(45) NOT NULL,
  `enderecoaluno` varchar(45) NOT NULL,
  `complementoaluno` varchar(45) DEFAULT NULL,
  `numeroaluno` varchar(45) NOT NULL,
  `cursoaluno` varchar(45) NOT NULL,
  `turmaaluno` varchar(45) NOT NULL,
  `paialuno` varchar(45) DEFAULT NULL,
  `profissaopai` varchar(45) DEFAULT NULL,
  `rgpai` varchar(45) DEFAULT NULL,
  `emailpai` varchar(45) DEFAULT NULL,
  `celularpai` varchar(45) DEFAULT NULL,
  `cpfpai` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idaluno`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='paialuno,profissaopai,rgpai,emailpai,"\n                + "celularpai,cpfpai';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbaluno`
--

LOCK TABLES `tbaluno` WRITE;
/*!40000 ALTER TABLE `tbaluno` DISABLE KEYS */;
INSERT INTO `tbaluno` VALUES (1,'03 /  26 / 2017','321.234.543 - 56','Joao Paulo da silva','11 / 12 / 1990','12.321.232','Masculino','Solteiro','Ativo','Joao@gmail.com','( 31 ) 9123 - 4567','belo horizonte','25 . 563 - 190','Sabará','casa engracada','rua dos bobos ','apt 2','56','Informatica','Informatica Noite A','Pedro tiago joao','pescador','1312121','pp@gmail.com','31 3333 3333','123123123'),(2,'11 /  11 / 1111','222.222.222 - 22','Naruto Uzumaki','33 / 33 / 3333','11.111.111','Masculino','Casado','Ativo','naruto@yahoo.com.br','( 31 ) 8765 - 4323','hong kong','13 . 244 - 556','Belo Horizonte','sayonara','rua Japao','casa A','235','Mineracao','Mineracao Noite A','Minato Hokage','Anime','345678122','minato@naruto.com','31 23456789','123123123'),(3,'26 /  03 / 2017','117.888.965 - 00','maria eugenia','03 / 12 / 1992','17.564.356','Feminino','A espera de um milagre','Inativo','maria123@hotmail.com.br','( 31 ) 3675 - 4455','brasileira','34 . 567 - 765','Sabará','coracao de jesus','rua del rey','apartamento','5','Informatica','Informatica Tarde A','Joao Paulo','Programador','12.123.521','joao@gmail.com','31 9 9263 7362','123.234.456-31');
/*!40000 ALTER TABLE `tbaluno` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-07 23:57:00
