-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: sistemaescolar
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbinstituicao`
--

DROP TABLE IF EXISTS `tbinstituicao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbinstituicao` (
  `id_inst` int(11) NOT NULL AUTO_INCREMENT,
  `nome_inst` varchar(45) NOT NULL,
  `cnpj_inst` varchar(45) NOT NULL,
  `endereco_inst` varchar(45) NOT NULL,
  `telefone_inst` varchar(45) NOT NULL,
  `cep_inst` varchar(45) NOT NULL,
  `bairro_inst` varchar(45) NOT NULL,
  `cidade_inst` varchar(45) NOT NULL,
  `email_inst` varchar(45) NOT NULL,
  `estado_inst` varchar(45) NOT NULL,
  `site_inst` varchar(45) NOT NULL,
  PRIMARY KEY (`id_inst`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbinstituicao`
--

LOCK TABLES `tbinstituicao` WRITE;
/*!40000 ALTER TABLE `tbinstituicao` DISABLE KEYS */;
INSERT INTO `tbinstituicao` VALUES (2,'Augustus','12345','Rua','36723455','34515460','campo','Sabara','augustus@','Minas Gerais','www.augustus.com'),(3,'senai','34567','rua 1','36789009','345152345','Siderurgica','sabara','senai@','Minas Gerais','senaimg.com'),(4,'Augustus','12345','Rua  sabara mg','36723455','34515460','campo','Sabara','augustus@','Minas Gerais','www.augustus.com');
/*!40000 ALTER TABLE `tbinstituicao` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-07 23:57:01
