-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: sistemaescolar
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbprofessor`
--

DROP TABLE IF EXISTS `tbprofessor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbprofessor` (
  `idprof` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nomeprof` varchar(100) NOT NULL,
  `enderecoprof` varchar(100) NOT NULL,
  `bairroprof` varchar(100) NOT NULL,
  `nascimentoprof` varchar(100) NOT NULL,
  `cepprof` varchar(100) NOT NULL,
  `cidadeprof` varchar(100) NOT NULL,
  `estadoprof` varchar(100) NOT NULL,
  `emailprof` varchar(100) NOT NULL,
  `rgprof` varchar(100) NOT NULL,
  `cpfprof` varchar(100) NOT NULL,
  `telefoneprof` varchar(100) NOT NULL,
  `celularprof` varchar(100) NOT NULL,
  `entradaprof` varchar(100) NOT NULL,
  `saidaprof` varchar(100) DEFAULT NULL,
  `disciplinaprof` varchar(100) NOT NULL,
  `statusprof` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idprof`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbprofessor`
--

LOCK TABLES `tbprofessor` WRITE;
/*!40000 ALTER TABLE `tbprofessor` DISABLE KEYS */;
INSERT INTO `tbprofessor` VALUES (4,'Luiz Fernando','Rua sem nome','Roca grande','12 / 12 / 1986','54162100','sabara','MG','Luiz Fernando@gmail.com','mg 39 002 381','584.476.233-91','31 9 1234 1234','31 9 3728 1734','11/11/1111','22/11/2222','todas','ativo');
/*!40000 ALTER TABLE `tbprofessor` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-07 23:57:01
